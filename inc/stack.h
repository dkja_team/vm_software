
#ifndef _STACK__
#define _STACK__

#include <array>
#include <stdexcept>


template<typename T, int SIZE=128>
class stack
{
    using value_type = T;

    std::array<value_type, SIZE> data;
    signed int                 last;

public:
    stack();
    stack(const stack& st);

    void push(const value_type& val);
    value_type pop();

    value_type top();
    bool empty() const;
    bool stacked() const;

};

template<typename T, int SIZE>
stack<T, SIZE>::stack()
    : data{},
    last{-1}
{
}

template<typename T, int SIZE>
stack<T,SIZE>::stack(const stack<T,SIZE>& st)
{
    this->data = st.data;
    this->last = st.last;
}


template<typename T, int SIZE>
void stack<T, SIZE>::push(const value_type& val)
{
        if(stacked())
            throw std::out_of_range{"stack out_of_range(when push())"};

        ++last;
        data.at(last) = val;
}


template<typename T, int SIZE>
auto stack<T, SIZE>::pop() -> value_type
{
    if (this->empty())
        throw std::out_of_range{"stack out_of_range(when pop())"};

    auto& r = data.at(last);
    --last;
    return r;
}


template<typename T, int SIZE>
bool stack<T, SIZE>::empty() const
{
    return (last==-1);
}

template<typename T, int SIZE>
bool stack<T, SIZE>::stacked() const
{
    return (last == SIZE-1);
}




#endif  // _STACK__
