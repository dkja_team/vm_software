#ifndef _CODE_CONTEINER__
#define _CODE_CONTEINER__

#include <fstream>
#include <vector>


using byte = unsigned char;


class Code_conteiner
{

    std::vector<byte> data;

public:
    Code_conteiner(std::ifstream& file);
    Code_conteiner(const std::vector<byte>& v);
    Code_conteiner(const Code_conteiner& c);
    Code_conteiner(Code_conteiner&& c);


    const std::vector<byte>& get_const_data() const;
    std::vector<byte>& get_data();
    byte* get_data_pointer() const;

    unsigned int get_dword(unsigned int ptr) const;
    unsigned short get_word(unsigned int ptr) const;
    byte    get_byte(unsigned int ptr) const;
};


#endif  // _CODE_CONTEINER__
