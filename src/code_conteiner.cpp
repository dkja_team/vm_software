#include "code_conteiner.h"

#include <utility>

Code_conteiner::Code_conteiner(std::ifstream& file)
    :data{}
{
    int tmp_ch{};

    while(file)
    {
        tmp_ch = file.get();
        if(tmp_ch == EOF)
            break;

        data.push_back(tmp_ch);
    }
}


Code_conteiner::Code_conteiner(const std::vector<byte>& v)
    :data{v}
{

}

Code_conteiner::Code_conteiner(const Code_conteiner& c)
    :data{c.data}
{
}

Code_conteiner::Code_conteiner(Code_conteiner&& c)
    :data{std::move(c.data)}
{
}


const std::vector<byte>& Code_conteiner::get_const_data() const
{
    return data;
}

std::vector<byte>& Code_conteiner::get_data()
{
    return data;
}

unsigned int Code_conteiner::get_dword(unsigned int ptr) const
{
    auto pointer = data.data();
    const unsigned int*pvalue = reinterpret_cast<const unsigned int*>(&(pointer[ptr]));
    return *pvalue;
}

unsigned short Code_conteiner::get_word(unsigned int ptr) const
{
    const unsigned short*pvalue = reinterpret_cast<const unsigned short*>(&(data.data()[ptr]));
    return *pvalue;
}
byte Code_conteiner::get_byte(unsigned int ptr) const
{
    const byte*pvalue = &(data.data()[ptr]);
    return *pvalue;
}
