#include <iostream>
#include "code_conteiner.h"
#include "stack.h"

using namespace std;

template <typename T, int SIZE>
ostream& operator<<(ostream& o, const stack<T, SIZE>& s)
{
    auto tmp = s;
    o << "stack<T,S>{";
    while(!tmp.empty())
        o << tmp.pop() << ' ';
    return o << '}';


}


int main()
{
    ifstream file{"file.txt"};
    if(!file)
        return -1;

    Code_conteiner code{file};

  //  cout << code.get_const_data();

    stack<int, 8> st0;


    cout << st0 << '\n';

    st0.push(5);

    cout << st0 << '\n';

    st0.push(2);

    cout << st0 << '\n';

    cout << "FROM STACK:\n"
        << st0.pop() << '\n'
        << st0.pop() << '\n'
        << st0.pop() << '\n';

    cout << st0 << '\n';

    return 0;
}
